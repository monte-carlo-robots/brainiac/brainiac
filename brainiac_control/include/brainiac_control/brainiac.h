//
// Created by henrique on 15/07/2020.
//


#ifndef BRAINIAC_CONTROL__BRAINIAC_H
#define BRAINIAC_CONTROL__BRAINIAC_H

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <brainiac_control/MotorCmd.h>
#include <brainiac_control/MotorEnc.h>
#include <regex>

namespace brainiac_control
{
/**
 * Encapsulation of the motor states and inputs.
 *
 * The fields are in should be filled with the angular values in S.I. units.
 * The fields are `position`, `velocity`, `torque`, and `voltage`.
 */
struct Motor{
  double position; ///< Motor position in rad.
  double velocity; ///< Motor velocity in rad/s.
  double torque; ///< Motor torque in N*m.
  double voltage; ///< Motor voltage in Volts.
};

struct SignedMotorCmd{
  int16_t right;
  int16_t left;
};

/**
 * Abstraction and interface of the real Brainiac motors system.
 *
 * The real robot has 2 motors, a left one and a right one. Thus, this class manages the states and commands using the
 * `Motor` struct and the motors' parameters. In addition, this class has to subscribe to the `MotorEnc` message coming
 * from the Brainiac Arduino and publish its MotorCmd.
 *
 * The `Brainiac::motorEncCb` has to be set as a subscriber after this object is instantiated. The `Brainiac::actuate`
 * method should be called whenever the voltage should be written to the motors.
 */
class Brainiac
{
  ros::Publisher left_sp_pub;
  ros::Publisher right_sp_pub;
  ros::Publisher left_state_pub;
  ros::Publisher right_state_pub;
  std_msgs::Float64 left_sp;
  std_msgs::Float64 right_sp;
  std_msgs::Float64 left_state;
  std_msgs::Float64 right_state;
  ros::Publisher pub;
  SignedMotorCmd sig_motor_cmd;
  MotorCmd motor_cmd;
  Motor left;
  Motor right;
  double ratio;
  double max_speed;
  double resistance;
  double k_a;
  double k_b;
  const double ref_voltage = 12;
  ros::Time last_enc_msg;
  double computeTorque(double velocity, double voltage) const;

public:
  /**
   * The callback to receive the `MotorEnc` message from the Arduino.
   *
   * Calculates the motors' position, velocity and torque based on the motor parameters. Then, updates (stores) these
   * values into the `Motor` struct.
   * @param msg The message received from the ros topic.
   */
  void motorEncCb(const MotorEnc::ConstPtr& msg);
  /**
   * The callback to receive the Control Effort message from the PID Controller.
   *
   * @param msg The message received from the ros topic.
   */
  void leftPIDCb(const std_msgs::Float64& msg);
  /**
   * The callback to receive the Control Effort message from the PID Controller.
   *
   * @param msg The message received from the ros topic.
   */
  void rightPIDCb(const std_msgs::Float64& msg);
  /**
   * Set the desired velocity for the left motor.
   *
   * Calculates the voltage to necessary to reach this velocity. Then, updates (stores) this value into the `Motor`
   * struct for the left motor.
   * @param velocity The desired velocity in rad/s.
   */
  void setLeftVel(double velocity);
  /**
   * Set the desired torque for the left motor.
   *
   * Calculates the voltage to necessary to reach this torque. Then, updates (stores) this value into the `Motor`
   * struct for the left motor.
   * @param torque The desired torque in N*m.
   */
  void setLeftTorque(double torque);
  /**
   * Get the `Motor` struct of the left motor.
   * @return The left `Motor` struct.
   */
  Motor getLeft();
  /**
   * Set the desired velocity for the right motor.
   *
   * Calculates the voltage to necessary to reach this velocity. Then, updates (stores) this value into the `Motor`
   * struct for the right motor.
   * @param velocity The desired velocity in rad/s.
   */
  void setRightVel(double velocity);
  /**
   * Set the desired torque for the right motor.
   *
   * Calculates the voltage to necessary to reach this torque. Then, updates (stores) this value into the `Motor`
   * struct for the right motor.
   * @param torque The desired torque in N*m.
   */
  void setRightTorque(double torque);
  /**
   * Get the `Motor` struct of the right motor.
   * @return The right `Motor` struct.
   */
  Motor getRight();
  /**
   * Sends the actual voltage of each motor converted into the `MotorCmd` msg through the publisher.
   */
  void actuate();

  /**
   * Instantiates the object reading the parameters from the ROS Parameters server and adds the publisher.
   * @param nh The ROS node handler where this object will publish and also have its subscriber registered after.
   */
  explicit Brainiac(ros::NodeHandle& nh);
};
} // end namespace brainiac_control

#endif //BRAINIAC_CONTROL__BRAINIAC_H
