//
// Created by henrique on 20/06/2020.
//

#ifndef BRAINIAC_CONTROL__BRAINIAC_HW_H
#define BRAINIAC_CONTROL__BRAINIAC_HW_H

#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <controller_manager/controller_manager.h>
#include <boost/scoped_ptr.hpp>
#include <ros/ros.h>

namespace brainiac_control{
/**
 * Abstraction of the hardware joints.
 *
 * This class has the attributes to deal with the type of the real robot joints. Nothing related with ROS nodes here.
 */
class BrainiacHW : public hardware_interface::RobotHW {
protected:
    // Interfaces
    hardware_interface::JointStateInterface joint_state_interface_;
    hardware_interface::VelocityJointInterface velocity_joint_interface_;

    joint_limits_interface::VelocityJointSaturationInterface velocity_joint_saturation_interface_;
    joint_limits_interface::VelocityJointSoftLimitsInterface velocity_joint_limits_interface_;

    // Shared memory
    int num_joints_;
    std::vector<std::string> joint_names_;
    std::vector<double> joint_position_;
    std::vector<double> joint_velocity_;
    std::vector<double> joint_effort_;
    std::vector<double> joint_velocity_command_;
    std::vector<double> joint_lower_limits_;
    std::vector<double> joint_upper_limits_;
    std::vector<double> joint_effort_limits_;
};
}

#endif //SRC_BRAINIAC_HARDWARE_H
