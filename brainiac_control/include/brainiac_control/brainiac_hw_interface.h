//
// Created by henrique on 20/06/2020.
//

#ifndef BRAINIAC_CONTROL__BRAINIAC_HW_INTERFACE_H
#define BRAINIAC_CONTROL__BRAINIAC_HW_INTERFACE_H

#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <controller_manager/controller_manager.h>
#include <boost/scoped_ptr.hpp>
#include <ros/ros.h>
#include <brainiac_control/brainiac_hw.h>
#include <brainiac_control/brainiac.h>

using namespace hardware_interface;
using joint_limits_interface::JointLimits;
using joint_limits_interface::SoftJointLimits;
using joint_limits_interface::PositionJointSoftLimitsHandle;
using joint_limits_interface::PositionJointSoftLimitsInterface;

namespace brainiac_control {

static const double POSITION_STEP_FACTOR = 10;
static const double VELOCITY_STEP_FACTOR = 10;

/**
 * The hardware joints interface with the ROS control.
 *
 * This class inherits the `BrainiacHW` and deals with the controller_manager, node handler, and the necessary updates.
 */
class BrainiacHWInterface : public BrainiacHW
{
public:
  /**
   * Instantiates the `BrainiacHWInterface`.
   *
   * Calls the `BrainiacHWInterface::init` method, and starts the ROS Control management.
   * @param nh The ROS node handler of the current node.
   * @param brainiac_ The real motors abstraction, used to read and write to the motors.
   */
  BrainiacHWInterface(ros::NodeHandle & nh, Brainiac *brainiac_);
  ~BrainiacHWInterface() override;
  /**
   * Starts all the Joints managements.
   *
   * Reads the joints properties from the ROS Parameters server. Then populates the class attributes related to those
   * properties accordingly. Also, the joints handler, limits, and others joints related are initialized here.
   *
   * Although public, you are not supposed to call it.
   */
  void init();
  /**
   * Function that is called in periodically to do the control loop.
   *
   * It calls the `BrainiacHWInterface::read` method, then the controller manager update, and finally the
   * `BrainiacHWInterface::write` method.
   * @param e
   *
   * Although public, you are not supposed to call it.
   */
  void update(const ros::TimerEvent &e);
  /**
   * Updates the joints status.
   */
  void read();
  /**
   * Send the velocity commands.
   */
  void write();

protected:
  Brainiac *brainiac = nullptr;
  ros::NodeHandle nh_;
  ros::Timer non_realtime_loop_;
  ros::Duration control_period_;
  ros::Duration elapsed_time_;
  double loop_hz_;
  boost::shared_ptr <controller_manager::ControllerManager> controller_manager_;
  double p_error_, v_error_, e_error_;
  std::regex left; ///> Regex to match any string with "left" insides it.
  std::regex right; ///> Regex to match any string with "right" insides it.
  int left_ind; ///> Index of the left motor.
  int right_ind; ///> Index of the right motor.
};
}

#endif //BRAINIAC_CONTROL__BRAINIAC_HW_INTERFACE_H
