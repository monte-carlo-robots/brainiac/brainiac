//
// Created by henrique on 15/07/2020.
//

#include <brainiac_control/brainiac.h>
#include <stdexcept>
#include <ros/console.h>


using namespace brainiac_control;

Brainiac::Brainiac(ros::NodeHandle & nh)
{
  ros::param::param("~max_speed", max_speed, 7.64454);
  ros::param::param("~ratio", ratio, 8344.0);
  ros::param::param("~resistance", resistance, 26.667);
  ros::param::param("~k_a", k_a, 0.00327);
  ros::param::param("~k_b", k_b, 0.0045);

  left_sp_pub = nh.advertise<std_msgs::Float64>("left_wheel/setpoint", 1);
  right_sp_pub = nh.advertise<std_msgs::Float64>("right_wheel/setpoint", 1);
  left_state_pub = nh.advertise<std_msgs::Float64>("left_wheel/state", 1);
  right_state_pub = nh.advertise<std_msgs::Float64>("right_wheel/state", 1);
  pub = nh.advertise<MotorCmd>("motors", 1);
  ROS_DEBUG_NAMED("Brainiac", "Brainiac object constructed.");
}
void Brainiac::motorEncCb(const MotorEnc::ConstPtr& msg)
{
  ROS_DEBUG_NAMED("Brainiac", "Brainiac callback being called.");
  MotorEnc enc_obj = *msg.get();

  auto old_left_pos = left.position;
  left.position = 2 * M_PI * (double) enc_obj.left / ratio;
  auto diff_left = (left.position - old_left_pos);
  left.velocity = diff_left ? diff_left / (enc_obj.header.stamp - last_enc_msg).toSec() : 0;
  left.torque = computeTorque(left.velocity, left.voltage);

  auto old_right_pos = right.position;
  right.position = 2 * M_PI * (double) enc_obj.right / ratio;
  right.velocity = (right.position - old_right_pos) / (enc_obj.header.stamp - last_enc_msg).toSec();
  right.torque = computeTorque(right.velocity, right.voltage);

  last_enc_msg = enc_obj.header.stamp;

  left_state.data = left.velocity;
  right_state.data = right.velocity;
  left_state_pub.publish(left_state);
  right_state_pub.publish(right_state);
}
void Brainiac::leftPIDCb(const std_msgs::Float64& msg)
{
  auto cmd = sig_motor_cmd.left + msg.data;
  motor_cmd.left = std::abs(cmd);
  motor_cmd.inverse_left = cmd < 0;
}
void Brainiac::rightPIDCb(const std_msgs::Float64& msg)
{
  auto cmd = sig_motor_cmd.right + msg.data;
  motor_cmd.right = std::abs(cmd);
  motor_cmd.inverse_right = cmd < 0;
}
void Brainiac::actuate()
{
  pub.publish(motor_cmd);
}
double Brainiac::computeTorque(double velocity, double voltage) const
{
  return (voltage - k_b * velocity) * k_a / resistance;
}
void Brainiac::setLeftVel(double velocity)
{
  left_sp.data = velocity;
  sig_motor_cmd.left = 255 * velocity / max_speed;
  left.voltage = ref_voltage * (double)sig_motor_cmd.left / 255.0;
  left_sp_pub.publish(left_sp);
}
void Brainiac::setLeftTorque(double torque)
{
  double ideal_voltage = k_b * left.velocity + std::abs(torque) * resistance / k_a;
  motor_cmd.left = 255 * std::min(ideal_voltage, ref_voltage) / ref_voltage;
  motor_cmd.inverse_left = torque < 0;
  left.voltage = ref_voltage * (double)sig_motor_cmd.left / 255.0;
}
Motor Brainiac::getLeft()
{
  return left;
}
void Brainiac::setRightVel(double velocity)
{
  right_sp.data = velocity;
  sig_motor_cmd.right = 255 * velocity / max_speed;
  right.voltage = ref_voltage * (double)sig_motor_cmd.right / 255.0;
  right_sp_pub.publish(right_sp);
}
void Brainiac::setRightTorque(double torque)
{
  double ideal_voltage = k_b * right.velocity + std::abs(torque) * resistance / k_a;
  motor_cmd.right = 255 * std::min(ideal_voltage, ref_voltage) / ref_voltage;
  motor_cmd.inverse_right = torque < 0;
  right.voltage = ref_voltage * (double)sig_motor_cmd.right / 255.0;
}
Motor Brainiac::getRight()
{
  return right;
}
