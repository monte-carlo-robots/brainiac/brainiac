//
// Created by henrique on 20/06/2020.
//

#include <sstream>
#include <brainiac_control/brainiac_hw_interface.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <joint_limits_interface/joint_limits_rosparam.h>

using namespace brainiac_control;
using joint_limits_interface::JointLimits;
using joint_limits_interface::SoftJointLimits;
using joint_limits_interface::PositionJointSoftLimitsHandle;

using joint_limits_interface::PositionJointSoftLimitsInterface;

BrainiacHWInterface::BrainiacHWInterface(ros::NodeHandle& nh, Brainiac *brainiac_) :
  nh_(nh), left("(.*)(left)(.*)"), right("(.*)(right)(.*)")
{
  brainiac = brainiac_;
  init();
  controller_manager_.reset(new controller_manager::ControllerManager(this, nh_));
  nh_.param("/brainiac/hardware_interface/loop_hz", loop_hz_, 0.1);
  ros::Duration update_freq = ros::Duration(1.0/loop_hz_);
  non_realtime_loop_ = nh_.createTimer(update_freq, &BrainiacHWInterface::update, this);
}

BrainiacHWInterface::~BrainiacHWInterface()
{
}

void BrainiacHWInterface::init()
{
  // Get joint names
  nh_.getParam("/brainiac/hardware_interface/joints", joint_names_);
  num_joints_ = joint_names_.size();
  if (num_joints_ != 2)
    throw ros::InvalidParameterException("Expected just 2 joints: One *right* and one *left*.");
  for (auto & joint_name : joint_names_){
    if (regex_match(joint_name, left) xor not regex_match(joint_name, right)){
      throw ros::InvalidParameterException("Expected the joints to have or left or right in its name.");
    }
  }
  left_ind = regex_match(joint_names_[0], left)? 0 : 1;
  right_ind = not left_ind;

  // Resize vectors
  joint_position_.resize(num_joints_);
  joint_velocity_.resize(num_joints_);
  joint_effort_.resize(num_joints_);
  joint_velocity_command_.resize(num_joints_);

  // Initialize Controller
  for (int i = 0; i < num_joints_; ++i) {

    // Create joint state interface
    JointStateHandle jointStateHandle(joint_names_[i], &joint_position_[i], &joint_velocity_[i], &joint_effort_[i]);
    joint_state_interface_.registerHandle(jointStateHandle);

      // Create position joint interface
//      JointHandle jointPositionHandle(jointStateHandle, &joint_position_command_[i]);
//      JointLimits limits;
//      SoftJointLimits softLimits;
//      getJointLimits(joint_names_[i], nh_, limits);
//      PositionJointSoftLimitsHandle jointLimitsHandle(jointPositionHandle, limits, softLimits);
//      positionJointSoftLimitsInterface.registerHandle(jointLimitsHandle);
//      position_joint_interface_.registerHandle(jointPositionHandle);

    // Create velocity joint interface
    JointHandle jointVelocityHandle(jointStateHandle, &joint_velocity_command_[i]);
    velocity_joint_interface_.registerHandle(jointVelocityHandle);
  }

  registerInterface(&joint_state_interface_);
  registerInterface(&velocity_joint_interface_);
}

void BrainiacHWInterface::update(const ros::TimerEvent& e) {
    elapsed_time_ = ros::Duration(e.current_real - e.last_real);
    read();
    controller_manager_->update(ros::Time::now(), elapsed_time_);
    write();
}

void BrainiacHWInterface::read() {
  auto left_motor = brainiac->getLeft();
  joint_position_[left_ind] = left_motor.position;
  joint_velocity_[left_ind] = left_motor.velocity;
  joint_effort_[left_ind] = left_motor.torque;

  auto right_motor = brainiac->getRight();
  joint_position_[right_ind] = right_motor.position;
  joint_velocity_[right_ind] = right_motor.velocity;
  joint_effort_[right_ind] = right_motor.torque;
}

void BrainiacHWInterface::write() {
//  if (elapsed_time.toSec() > 0.0){
//    positionJointSoftLimitsInterface.enforceLimits(elapsed_time);
//  }
  brainiac->setLeftVel(joint_velocity_command_[left_ind]);
  brainiac->setRightVel(joint_velocity_command_[right_ind]);
  brainiac->actuate();
}
