//
// Created by henrique on 20/06/2020.
//
#include <brainiac_control/brainiac_hw_interface.h>
#include <brainiac_control/brainiac.h>
#include <ros/callback_queue.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "brainiac_hw_interface");
  ros::CallbackQueue ros_queue;

  ros::NodeHandle nh;
  nh.setCallbackQueue(&ros_queue);
  brainiac_control::Brainiac brainiac(nh);
  brainiac_control::BrainiacHWInterface brainiac_hw_interface(nh, &brainiac);
  ros::Subscriber sub = nh.subscribe("wheels", 1, &brainiac_control::Brainiac::motorEncCb, &brainiac);
  ros::Subscriber left_pid_sub = nh.subscribe("/left_wheel/control_effort", 1, &brainiac_control::Brainiac::leftPIDCb, &brainiac);
  ros::Subscriber right_pid_sub = nh.subscribe("/right_wheel/control_effort", 1, &brainiac_control::Brainiac::rightPIDCb, &brainiac);

  ros::MultiThreadedSpinner spinner(0);
  spinner.spin(&ros_queue);
  return 0;
}
