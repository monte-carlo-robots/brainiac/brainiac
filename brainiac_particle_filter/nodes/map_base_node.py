#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from nav_msgs.srv import GetMap
from brainiac_particle_filter.map import Map

if __name__ == "__main__":
    rospy.init_node("particle_filter")
    rospy.wait_for_service("static_map")
    map_msg = rospy.ServiceProxy("static_map", GetMap)().map
    map_instance = Map(map_msg)
    print(map_instance.get_by_world(-3.4697, 4.5))
    print(map_instance.get_by_index(1, 9))
    # map_instance.plot()
    rospy.spin()
