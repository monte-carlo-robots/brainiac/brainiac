#!/usr/bin/env python

# class Map
from geometry_msgs.msg import Point
import matplotlib.pyplot as plt
import numpy as np


class Map:
    def __init__(self, grid_map, frame):
        self.frame = frame
        self.map = grid_map
        self.width = grid_map.info.width
        self.height = grid_map.info.height
        self.resolution = grid_map.info.resolution
        self.grid_data = np.array(self.map.data, dtype=np.int8).\
            reshape(self.height, self.width)

        self.origin = Point()
        self.origin.x = grid_map.info.origin.position.x
        self.origin.y = grid_map.info.origin.position.y
        self.vec_origin = np.array([self.origin.x, self.origin.y], dtype=np.float)
        def dummy(occ_map_x, occ_map_y):
            return self.are_indices_in_range(occ_map_x, occ_map_y)
        self.vec_are_indices_in_range = np.vectorize(dummy)

    def are_indices_in_range(self, occ_map_x, occ_map_y):
        return -1 < occ_map_x < self.width and -1 < occ_map_y < self.height

    def get_by_index(self, occ_map_x, occ_map_y):
        if self.are_indices_in_range(occ_map_x, occ_map_y):
            # data comes in row-major order http://docs.ros.org/en/melodic/api/nav_msgs/html/msg/OccupancyGrid.html
            return self.grid_data[occ_map_y, occ_map_x]
        else:
            raise IndexError(
                "Coordinates out of grid_map, x: {}, y: {} must be in between: [0, {}], [0, {}]".format(
                    occ_map_x, occ_map_y, self.height, self.width))

    def vec_get_by_index(self, cindex):
        x = cindex[:, 0]
        y = cindex[:, 1]
        if self.vec_are_indices_in_range(x, y).all():
            # data comes in row-major order http://docs.ros.org/en/melodic/api/nav_msgs/html/msg/OccupancyGrid.html
            return self.grid_data[y, x]
        else:
            raise IndexError(
                "Coordinates out of grid_map, x: {}, y: {} must be in between: [0, {}], [0, {}]".format(
                    x, y, self.height, self.width))

    def is_in_occ_map(self, x, y):
        return -1 < x < self.width and -1 < y < self.height

    def get_by_world(self, world_x, world_y):
        cx, cy = self.get_indices(world_x, world_y)
        try:
            return self.get_by_index(cx, cy)
        except IndexError as e:
            raise IndexError(
                "Coordinates out of grid x: {}, y: {} must be in between: [{}, {}], [{}, {}]. Internal error: {}".format(
                    world_x, world_y,
                    self.origin.x,
                    self.origin.x + self.height * self.resolution,
                    self.origin.y,
                    self.origin.y + self.width * self.resolution,
                    e))
        
    def vec_get_by_world(self, particles, cache_trash):
        particles = particles[:, 0:2]
        cindex = self.vec_get_indices(particles, cache_trash)
        return self.vec_get_by_index(cindex)

    def get_world_x_y(self, occ_map_x, occ_map_y):
        world_x = occ_map_x * self.resolution + self.origin.x
        world_y = occ_map_y * self.resolution + self.origin.y
        return world_x, world_y

    def get_indices(self, world_x, world_y):
        occ_map_x = np.clip(int((world_x - self.origin.x) / self.resolution), 0, self.height)
        occ_map_y = np.clip(int((world_y - self.origin.y) / self.resolution), 0, self.width)
        return occ_map_x, occ_map_y

    def vec_get_indices(self, particles, cache_trash):
        np.subtract(particles, self.vec_origin, out=cache_trash)
        np.divide(cache_trash, self.resolution, out=cache_trash)
        cache_trash = cache_trash.astype(np.int)
        np.clip(cache_trash, [0, 0], [self.height, self.width], out=cache_trash)
        return cache_trash

    def plot(self):
        plt.pcolor(self.grid_data, cmap='binary', vmin=0, vmax=100)
        plt.show()

