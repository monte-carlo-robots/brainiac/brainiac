#!/usr/bin/env python

# packages
import numpy as np
from threading import Thread
import utils as Utils
from copy import deepcopy

# messages
from geometry_msgs.msg import PoseArray, PoseWithCovarianceStamped, Point, Pose


class ParticleFilter(object):
    """ The class that represents a Particle Filter ROS Node """

    MAX_PARTICLES = 100000
    RVIZ = False

    def __init__(self, map_instance, logger, theta, theta_cov):

        self.logger = logger
        self.__theta_imu = theta
        self.__theta_imu_cov = theta_cov
        # The sensor model buffer
        self.sensor_data = None

        # initialize data containers used in the MCL algorithm
        self.odom_cov = np.zeros((3, 3), np.float)

        # initialize data containers for map instance
        self.map_handler = map_instance

        # Cached values to avoid reprocessing
        self.CACHED_TRASH_RESULT_MEMORY_ALLOC2 = np.zeros((self.MAX_PARTICLES, 2))
        self.CACHED_TRASH_RESULT_MEMORY_ALLOC = np.zeros(self.MAX_PARTICLES)
        self.CACHED_ONES_MAX_PARTICLES = np.ones(self.MAX_PARTICLES)
        self.UNIFORM_WEIGHTS = self.CACHED_ONES_MAX_PARTICLES / float(self.MAX_PARTICLES)

        # initialize particle poses array and weights
        self.__inferred_pose = PoseWithCovarianceStamped()
        self.particle_indices = np.arange(self.MAX_PARTICLES)
        self.particles = np.zeros((self.MAX_PARTICLES, 3), dtype=np.float16)
        self.weights = self.UNIFORM_WEIGHTS.copy()
        self.particle_cloud = np.zeros((self.MAX_PARTICLES, 3))
        self.grid = self.CACHED_ONES_MAX_PARTICLES.copy()

        # initialize last_pose and actual_pose to all zeros
        self.last_pose = None
        self.actual_pose = Pose()

        # External functions
        self.pose_pub = None
        self.make_header = None
        self.request_sensor_msg = None
        self.particle_publisher = None

        # Visualization thread
        self.thread = None

        # these topics are for coordinate space transformations
        self.__sensor_tf = None
        self.__sensor_tf_inv = None

        # initialize particle cloud
        self.initialize_global()

        self.logger.loginfo("Finished initializing, waiting messages...")

    def visualize(self):
        """ Publish various visualization messages. """
        if not self.RVIZ or (self.thread and self.thread.is_alive()):
            return None

        self.thread.join() if self.thread else None
        self.thread = Thread(target=self.publish_particles)
        self.thread.start()

    def publish_particles(self):
        self.logger.loginfo("Publishing particles to RVIZ")

        header = self.make_header(self.map_handler.frame)
        particle_cloud = Utils.vector_particle_to_pose(self.particles[:, 0], self.particles[:, 1], self.particles[:, 2])

        pose_array_data = PoseArray(header=header, poses=particle_cloud)
        self.particle_publisher.publish(pose_array_data)

    def odom_callback(self, msg):
        """ Store deltas between consecutive odom messages in the coordinate space. """
        msg = deepcopy(msg)
        # cov = msg.pose.covariance
        # Utils.mat_cov_from_plain(cov, self.odom_cov)
        # self.odom_cov[0] = cov[0:2] + cov[5]
        # self.odom_cov[1] = cov[6:8] + cov[11]
        # self.odom_cov[2] = cov[30:32] + cov[35]

        self.odom_cov[0, 0] = 0.000025 * 10
        self.odom_cov[1, 1] = 0.000025 * 10
        self.odom_cov[2, 2] = 0.000025 * 10

        self.actual_pose = msg.pose.pose
        if self.last_pose is not None:
            self.update()
        self.last_pose = self.actual_pose

    def start_pose_callback(self, msg):
        """ Receive pose messages from RViz and initialize the particle distribution in response. """
        if isinstance(msg, PoseWithCovarianceStamped):
            self.initialize_particles_pose(msg.pose)
        else:
            self.logger.logdebug("Provide a initial pose data with type: PoseWithCovarianceStamped")

    def initialize_particles_pose(self, msg):
        """ Initialize particles in the general region of the provided pose. """
        self.logger.logdebug("Pose initialization")
        pose = msg.pose
        pose_covariance_matrix = Utils.mat_cov_from_plain(msg.covariance)
        self.weights = self.UNIFORM_WEIGHTS.copy()
        vector_coordinates = np.random.normal(loc=[0, 0, 0], scale=pose_covariance_matrix, size=self.MAX_PARTICLES)
        self.particles[:, 0] = pose.position.x + vector_coordinates[:, 0]
        self.particles[:, 1] = pose.position.y + vector_coordinates[:, 1]
        self.particles[:, 2] = Utils.quaternion_to_angle(pose.orientation) + vector_coordinates[:, 2]
        self.clip_particles()

    def clip_particles(self):
        a_min = [self.map_handler.origin.x,
                 self.map_handler.origin.y]
        a_max = [self.map_handler.height * self.map_handler.resolution + self.map_handler.origin.x,
                 self.map_handler.width * self.map_handler.resolution + self.map_handler.origin.y]
        elements = np.argwhere(np.logical_or(a_min >= self.particles[:, :2], self.particles[:, :2] >= a_max))
        self.weights[elements] = 0
        self.normalize_weights()
        self.resample()

    def initialize_global(self):
        """ Spread the particle distribution over map state space. """
        self.logger.logdebug("Global pose initialization")
        self.particles[:, 0] = np.random.uniform(low=self.map_handler.origin.x,
                                                 high=self.map_handler.origin.x +
                                                 self.map_handler.height * self.map_handler.resolution,
                                                 size=self.MAX_PARTICLES)
        self.particles[:, 1] = np.random.uniform(low=self.map_handler.origin.y,
                                                 high=self.map_handler.origin.y +
                                                 self.map_handler.width * self.map_handler.resolution,
                                                 size=self.MAX_PARTICLES)
        self.particles[:, 2] = np.random.normal(loc=self.__theta_imu,
                                                scale=self.__theta_imu_cov,
                                                size=self.MAX_PARTICLES)
        self.weights = self.UNIFORM_WEIGHTS.copy()


    def motion_model(self):
        """ The motion model applies the motion from odom reference to each particle. """
        # T_sensor^-1 * T1^-1 * noise * T2 * T_sensor = T
        # Tp2 = Tp1 * T
        noise_vector = np.random.multivariate_normal(
            (0.0, 0.0, 0.0),
            self.odom_cov,
            self.MAX_PARTICLES
        )
        t2 = [
            self.actual_pose.position.x,
            self.actual_pose.position.y,
            Utils.quaternion_to_angle(self.actual_pose.orientation)
        ]
        t1 = [
            self.last_pose.position.x,
            self.last_pose.position.y,
            Utils.quaternion_to_angle(self.last_pose.orientation)
        ]

        self.particles = Utils.vector_transform_2d(
            Utils.vec_transform_2d(
                self.particles,
                Utils.transform_2d(
                    self.__sensor_tf_inv,
                    Utils.inv_2d(t1)
                ),
            ),
            Utils.vec_transform_2d(
                noise_vector,
                Utils.transform_2d(
                    t2,
                    self.__sensor_tf
                )
            )
        )

        self.clip_particles()

    def sensor_model(self):
        """ Compute the sensor model based on the data received from the light sensor with
        range [0-255]"""
        if self.sensor_data is not None:
            last_sensor_value = self.sensor_data / 255
            occupancy_grid_values = self.map_handler.vec_get_by_world(
                self.particles, self.CACHED_TRASH_RESULT_MEMORY_ALLOC2
            )
            self.grid[occupancy_grid_values == 100] = 0

            # The formula implemented by the following lines is: W = W * (1 - |map - sensor|)
            np.subtract(self.grid, last_sensor_value, out=self.CACHED_TRASH_RESULT_MEMORY_ALLOC)
            np.abs(self.CACHED_TRASH_RESULT_MEMORY_ALLOC, out=self.CACHED_TRASH_RESULT_MEMORY_ALLOC)
            np.subtract(self.CACHED_ONES_MAX_PARTICLES, self.CACHED_TRASH_RESULT_MEMORY_ALLOC,
                        out=self.CACHED_TRASH_RESULT_MEMORY_ALLOC)
            np.multiply(self.weights, self.CACHED_TRASH_RESULT_MEMORY_ALLOC, out=self.weights)

            self.grid = self.CACHED_ONES_MAX_PARTICLES.copy()
            self.normalize_weights()
            self.sensor_data = None
        else:
            self.logger.logdebug("No sensor data received.")

    def mcl(self):
        """
        Performs one step of Monte Carlo Localization.
            1. apply the motion model
            2. apply the sensor model
            3. generate vector of random particles
            4. normalize particle weights
        """
        # compute the motion model to update the proposal distribution
        self.motion_model()

        # compute the sensor model
        self.sensor_model()

        # resample the particles based on the weights values
        self.resample()

        # reset the importance weights
        self.weights = self.UNIFORM_WEIGHTS.copy()

    def resample(self):
        proposal_indices = np.random.choice(self.particle_indices, self.MAX_PARTICLES, p=self.weights)
        self.particles = self.particles[proposal_indices, :]

    @property
    def sensor_tf(self):
        return self.__sensor_tf

    @sensor_tf.setter
    def sensor_tf(self, value):
        self.__sensor_tf = value
        self.__sensor_tf_inv = Utils.inv_2d(self.__sensor_tf)

    @property
    def inferred_pose(self):
        # returns the expected value of the pose given the particle distribution
        mean_pose = np.mean(self.particles, axis=0)
        # transform inferred pose from sensor to rotational_link
        mean_pose = Utils.transform_2d(
            mean_pose, self.__sensor_tf_inv
        )
        matrix_ones = np.ones((self.MAX_PARTICLES, 3))
        deviation_scores = (self.particles - mean_pose * matrix_ones)
        covariance = np.dot(np.transpose(deviation_scores), deviation_scores) * (1.0 / self.MAX_PARTICLES)
        self.__inferred_pose.header = self.make_header("odom")
        self.__inferred_pose.pose.pose.position = Point(mean_pose[0], mean_pose[1], 0)
        self.__inferred_pose.pose.pose.orientation = Utils.angle_to_quaternion(mean_pose[-1])
        self.__inferred_pose.pose.covariance = Utils.plain_cov_from_mat(covariance)
        return self.__inferred_pose

    def update_sensor_data(self):
        """ Update light sensor data"""
        self.sensor_data = self.request_sensor_msg().data

    def normalize_weights(self):
        """ Make sure the particle weights define a valid distribution (i.e. sum to 1.0) """
        if np.sum(self.weights) == 0:
            self.weights = self.UNIFORM_WEIGHTS.copy()
            self.logger.logwarn("ALL ZEROS! \n")
            return None
        self.weights = self.weights / np.sum(self.weights)
        while np.isnan(self.weights).any():
            if np.isnan(self.weights).all():
                self.initialize_global()
                break
            self.weights = np.where(np.isnan(self.weights), np.nanmin(self.weights)/10, self.weights) # TODO: optimize it
            self.weights = self.weights / np.sum(self.weights)
            self.logger.logwarn("Inside loop! \n %s \n" %(str(self.weights)))

    def update(self):
        """ Apply the MCL function to update particle filter state. """
        self.update_sensor_data()

        # run the MCL update algorithm
        self.mcl()

        # publish inferred pose frame based on inferred pose
        self.pose_pub.publish(self.inferred_pose)

        self.visualize()
