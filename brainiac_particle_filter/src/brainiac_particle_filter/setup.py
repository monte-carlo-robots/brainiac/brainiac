# python setup.py build_ext --inplace

import numpy as np
from setuptools import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext


setup(
    name="My hello app",
    ext_modules=cythonize("*.pyx", include_path=[np.get_include()]),
    cmdclass={'build_ext': build_ext},
    zip_safe=False,
)

