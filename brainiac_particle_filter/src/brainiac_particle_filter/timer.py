from time import sleep, time
from timeit import timeit
import csv

from particle_filter import ParticleFilter as ParentClass

class Timer(ParentClass):
    def __init__(self, *args):
        self.time_dict = dict()
        super(Timer, self).__init__(*args)

    def __getattribute__(self, item):
        try:
            met = object.__getattribute__(self, item)
            def fun(*args):
                t1 = time()
                res = met(*args)
                t2 = time()
                elapsed_time = t2 - t1
                if item in self.time_dict:
                    self.time_dict[item].append(elapsed_time)
                else:
                    self.time_dict[item] = [t2-t1]
                return res
            if callable(met):
                return fun
            else:
                return met

        except Exception, error:
            raise error

    def generate_table(self):
        header = ['Method', 'Total time', 'Number of runs', 'Average time']
        rows = []
        for method in self.time_dict.keys():
            time_list = self.time_dict[method]
            total_time = sum(time_list)
            number_of_runs = len(time_list)
            avg_time = total_time / number_of_runs
            rows.append([method, total_time, number_of_runs, avg_time])
        return header, rows

    def save_csv(
            self,
            csv_file="log_timer_%d.csv"%time()
    ):
        header, rows = self.generate_table()
        print "writing"
        with open(csv_file, 'wb') as filename:
            writer = csv.writer(filename)
            writer.writerow(header)
            writer.writerows(rows)

    def __del__(self):
        self.save_csv()
