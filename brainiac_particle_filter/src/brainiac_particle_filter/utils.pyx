import rospy
import numpy as np

from tf import transformations
from tf.transformations import quaternion_matrix, quaternion_multiply, quaternion_inverse, inverse_matrix
from std_msgs.msg import Header
from geometry_msgs.msg import Point, Pose, Quaternion
from nav_msgs.msg import Odometry
from transformations import euler_from_matrix
import math


def angle_to_quaternion(angle):
    """Convert an angle in radians into a quaternion """
    return Quaternion(*transformations.quaternion_from_euler(0, 0, angle))


def quaternion_to_angle(q):
    """ Convert a quaternion _message_ into an angle in radians. The angle represents the yaw. """
    x, y, z, w = q.x, q.y, q.z, q.w
    roll, pitch, yaw = transformations.euler_from_quaternion((x, y, z, w))
    return yaw


def particle_to_pose(particle_data_x, particle_data_y, particle_data_theta):
    """ Converts a particle in the form [x, y, theta] into a Pose object """
    pose = Pose()
    pose.position = Point(particle_data_x, particle_data_y, 0)
    pose.orientation = angle_to_quaternion(particle_data_theta)
    return pose


# TODO: Should be on map class
def coord_to_indices(position, origin, resolution, map_dimension):
    return np.clip(int((position - origin) / resolution), 0, map_dimension - 1)


vector_particle_to_pose = np.vectorize(particle_to_pose)
vector_coord_to_indices = np.vectorize(coord_to_indices)  # TODO: Remove this vectorize


# TODO: Separate it, class and library shouldn't be together
def get_param_type_check(param, default):
    """
    Get the ROS parameter and asserts if its type correspond to the default value type.
    :param param: The param name
    :param default: The default value
    :return: The param value
    """
    expected_type = type(default)
    value = rospy.get_param(param, default)
    # todo: problems with assertion
    # assert isinstance(value, expected_type), ParticleFilter.ERROR_MSG % (param, expected_type.__name__)
    return value


# TODO: OPTIMIZATION: If needed, the returned array should be cached
def odom_to_coordinates(odom):
    """ Converts an Odometry message into [x, y, theta] coordinate. """
    orientation = quaternion_to_angle(odom.pose.pose.orientation)
    return np.array([odom.pose.pose.position.x, odom.pose.pose.position.y, orientation])


# TODO: Remove this function // TODO Remove
def map_to_world(poses, map_info):
    """
    Takes a two dimensional numpy array of poses:
        [ [x0,y0,theta0], ... ]
    And converts them from map coordinate space (pixels) to world coordinate space (meters).
    """
    # todo: refactor
    scale = map_info.resolution
    angle = quaternion_to_angle(map_info.origin.orientation)

    # rotation -> could be done with rotation matrix
    c, s = np.cos(angle), np.sin(angle)

    # store temp coordinates since they will be overwritten
    temp = np.copy(poses[:, 0])
    poses[:, 0] = c * poses[:, 0] - s * poses[:, 1]
    poses[:, 1] = s * temp + c * poses[:, 1]

    # scale
    poses[:, :2] *= float(scale)

    # translate
    poses[:, 0] += map_info.origin.position.x
    poses[:, 1] += map_info.origin.position.y
    poses[:, 2] += angle


def mat_cov_from_plain(vector_cov, output_matrix=np.ones((3, 3))):
    """
    Extracts the covariance matrix from a flatten covariance msg
    @param vector_cov: flatten cov matrix
    @param output_matrix: writes the covariance matrix on it
    """
    output_matrix[0] = vector_cov[0:2] + [vector_cov[5]]
    output_matrix[1] = vector_cov[6:8] + [vector_cov[11]]
    output_matrix[2] = vector_cov[30:32] + [vector_cov[35]]
    return output_matrix


def plain_cov_from_mat(matrix_cov, output_array=np.ones(36)):
    """
    Extracts the flatten array from a covariance matrix
    @param matrix_cov: covariance matrix
    @param output_array: flatten array from covariance matrix
    @return: writes the covariance flatten array on it
    """
    output_array[0:2] = matrix_cov[0, 0:2]
    output_array[5] = matrix_cov[0, -1]
    output_array[6:8] = matrix_cov[1, 0:2]
    output_array[11] = matrix_cov[1, -1]
    output_array[30:32] = matrix_cov[2, 0:2]
    output_array[35] = matrix_cov[2, -1]
    return output_array


def pose_to_transform_matrix(position, quaternion):
    h_transform = quaternion_matrix([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
    h_transform[:3, 3] = np.array([position.x, position.y, position.z])
    return h_transform


def particle_h_transform(x, y, theta):
    c0 = np.cos(theta)
    s0 = np.sin(theta)
    return np.array([
        [c0, -s0, 0, x],
        [s0, c0, 0, y],
        [0,   0, 1, 0],
        [0,   0, 0, 1]
    ])


def h_transform_diff(pose1, pose2):
    """
    T2 = T1 * T
    T = T1^-1 * T2
    """
    h_transform1 = pose_to_transform_matrix(pose1.pose.position, pose1.pose.orientation)
    h_transform2 = pose_to_transform_matrix(pose2.pose.position, pose2.pose.orientation)
    h_transform1_inv = np.linalg.inv(h_transform1)
    h_transform_diff_data = np.matmul(h_transform1_inv, h_transform2)
    return h_transform_diff_data


def vector_transform_2d(particle1, particle2):
    """
    For 2D Homogeneous Transformations is equivalent to:

    particle_from_h_transform(
        particle_h_transform(*particle1) @ particle_h_transform(*particle2)
    )
    """
    trash_res = np.zeros(particle1.shape[0])
    trash_res2 = np.zeros(particle1.shape[0])
    res = np.zeros(particle1.shape)

    s0 = np.sin(particle1[:, 2])
    c0 = np.cos(particle1[:, 2])

    np.multiply(c0, particle2[:, 0], out=trash_res)
    np.multiply(s0, particle2[:, 1], out=trash_res2)
    np.subtract(trash_res, trash_res2, out=trash_res)
    np.add(trash_res, particle1[:, 0], out=res[:, 0])

    np.multiply(s0, particle2[:, 0], out=trash_res)
    np.multiply(c0, particle2[:, 1], out=trash_res2)
    np.add(trash_res, trash_res2, out=trash_res)
    np.add(trash_res, particle1[:, 1], out=res[:, 1])

    np.add(particle2[:, 2], particle1[:, 2], out=res[:, 2])
    return res


def vec_transform_2d(particle1, particle2):
    """
    For 2D Homogeneous Transformations is equivalent to:

    particle_from_h_transform(
        particle_h_transform(*particle1) @ particle_h_transform(*particle2)
    )
    """
    trash_res = np.zeros(particle1.shape[0])
    trash_res2 = np.zeros(particle1.shape[0])
    res = np.zeros(particle1.shape)

    s0 = np.sin(particle1[:, 2])
    c0 = np.cos(particle1[:, 2])

    np.multiply(c0, particle2[0], out=trash_res)
    np.multiply(s0, particle2[1], out=trash_res2)
    np.subtract(trash_res, trash_res2, out=trash_res)
    np.add(trash_res, particle1[:, 0], out=res[:, 0])

    np.multiply(s0, particle2[0], out=trash_res)
    np.multiply(c0, particle2[1], out=trash_res2)
    np.add(trash_res, trash_res2, out=trash_res)
    np.add(trash_res, particle1[:, 1], out=res[:, 1])

    np.add(particle2[2], particle1[:, 2], out=res[:, 2])
    return res


def transform_2d(particle1, particle2):
    """
    For 2D Homogeneous Transformations is equivalent to:

    particle_from_h_transform(
        particle_h_transform(*particle1) @ particle_h_transform(*particle2)
    )
    """
    s0 = np.sin(particle1[2])
    c0 = np.cos(particle1[2])
    return np.array([
        c0 * particle2[0] - s0 * particle2[1] + particle1[0],
        s0 * particle2[0] + c0 * particle2[1] + particle1[1],
        particle2[2] + particle1[2]
    ])


def inv_2d(particle):
    s0 = np.sin(particle[2])
    c0 = np.cos(particle[2])
    return - c0 * particle[0] - s0 * particle[1], s0 * particle[0] - c0 * particle[1], - particle[2]


def matmul_elements(*args, **kwargs):
    out = kwargs['out']
    np.matmul(args[0], args[1], out=out)
    for i in range(2, len(args)):
        np.matmul(out, args[i], out=out)


def pose_from_h_transform(h_transform, pose_buffer):
    """
    Receives a Homogeneous transformation and insert the pose with euler angles
    in the pose_buffer
    """
    euler_rad_angles = np.array(euler_from_matrix(h_transform, 'rxyz'))
    euler_deg_angles = np.array(map(math.degrees, euler_rad_angles))
    pose_buffer[0] = h_transform[0, 3]
    pose_buffer[1] = h_transform[1, 3]
    pose_buffer[2] = h_transform[2, 3]
    pose_buffer[3] = euler_deg_angles[0]
    pose_buffer[4] = euler_deg_angles[1]
    pose_buffer[5] = euler_deg_angles[2]

def quaternion_to_vector(quaternion):
    return np.array([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
