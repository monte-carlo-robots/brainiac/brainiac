PKG = "brainiac_particle_filter"
NAME = "map_test"

import roslib

import sys
import unittest

from geometry_msgs.msg import Pose, Point, Quaternion
from nav_msgs.msg import OccupancyGrid
from brainiac_particle_filter.map import Map


class TestMap(unittest.TestCase):
    def setUp(self):
        self.occupancy_grid = OccupancyGrid()
        self.occupancy_grid.info.resolution = 0.1
        self.occupancy_grid.info.width = 500
        self.occupancy_grid.info.height = 500
        self.occupancy_grid.info.origin = Pose(position=Point(5.0, 5.0, 0.0),
                                               orientation=Quaternion(0, 0, 0, 1))
        self.occupancy_grid.data = list(range(self.occupancy_grid.info.height *
                                              self.occupancy_grid.info.width *
                                              int((1 / self.occupancy_grid.info.resolution))))
        self.map_instance = Map(self.occupancy_grid)

    def test_parameters(self):
        self.assertIsInstance(self.map_instance.map, OccupancyGrid)
        self.assertEqual(self.map_instance.width, self.occupancy_grid.info.width)
        self.assertEqual(self.map_instance.height, self.occupancy_grid.info.height)
        self.assertEqual(self.map_instance.resolution, self.occupancy_grid.info.resolution)
        self.assertEqual(self.map_instance.origin.x, self.occupancy_grid.info.origin.position.x)
        self.assertEqual(self.map_instance.origin.y, self.occupancy_grid.info.origin.position.y)

    def test_data(self):
        self.assertEqual(len(self.map_instance.map.data), 500 * 500 * int(1 / 0.1))
        self.assertListEqual(self.map_instance.map.data, list(range(500 * 500 * int(1 / 0.1))))

    def test_get_by_index(self):
        self.assertEqual(self.map_instance.get_by_index(0, 30), 30)
        self.assertEqual(self.map_instance.get_by_index(1, 400), 900)
        self.assertEqual(self.map_instance.get_by_index(2, 300), 1300)
        self.assertRaises(IndexError, lambda: self.map_instance.get_by_index(-1, 300))
        self.assertRaises(IndexError, lambda: self.map_instance.get_by_index(0, 500))

    def test_get_by_coord(self):
        pass
        # todo: change assert value
        # self.assertEqual(self.map_instance.coord_to_indices(35, 30), 125299)
        # self.assertEqual(self.map_instance.coord_to_indices(40, 15), 175099)

    def test_coord_to_indices(self):
        self.assertEqual(self.map_instance.coord_to_indices(35, 30), (250, 300))
        self.assertEqual(self.map_instance.coord_to_indices(40, 15), (100, 350))

    def test_are_indices_in_range(self):
        self.assertTrue(self.map_instance.are_indices_in_range(2, 300))
        self.assertFalse(self.map_instance.are_indices_in_range(-1, 300))


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(PKG, NAME, TestMap)
