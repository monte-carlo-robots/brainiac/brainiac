PKG = "brainiac_particle_filter"
NAME = "particle_filter_test"

import roslib

import sys
import unittest
import time
from numpy.testing import *

from geometry_msgs.msg import Pose, Point, Quaternion
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import Illuminance
from std_msgs.msg import Header

from brainiac_particle_filter.particle_filter import *
from brainiac_particle_filter.map import Map


def make_header(frame_id, stamp=None):
    """ Creates a Header object for stamped ROS objects """
    header = Header()
    header.stamp = stamp
    header.frame_id = frame_id
    return header


def wait_sensor_msg():
    """ Simulates the sensor behavior for asynchronous messages"""
    time.sleep(1)
    il = Illuminance()
    il.illuminance = 123
    return il


class Logger(object):
    def logdebug(self, data):
        print "DEBUG: " + str(data)

    def loginfo(self, data):
        print "INFO: " + str(data)

    def logwarn(self, data):
        print "WARN: " + str(data)

    def logerr(self, data):
        print "ERR: " + str(data)

    def logfatal(self, data):
        print "FATAL: " + str(data)


class Publisher(object):
    """ Simulates the ROS Publisher behavior """
    def __init__(self):
        self.buffer = []

    def publish(self, value):
        self.buffer.append(value)


class TestParticleFilter(unittest.TestCase):
    def setUp(self):
        grid = OccupancyGrid()
        grid.info.resolution = 1
        grid.info.width = 5
        grid.info.height = 5
        grid.info.origin = Pose(position=Point(0.0, 0.0, 0.0),
                                orientation=Quaternion(0, 0, 0, 1))
        grid.data = [100, 0, 0, 0, 100,
                     0, 100, 0, 0, 100,
                     0, 100, 0, 0, 100,
                     0, 0, 100, 0, 100,
                     0, 0, 0, 100, 100]
        l = []
        self.mask = (100 - np.array(grid.data, dtype=np.float)) / 100.
        for i in range(grid.info.height):
            for j in range(grid.info.height):
                l.append([i + 0.5, j + 0.5, 0])
        self.particles_static = np.array(l)
        self.map_ = Map(grid)

        ParticleFilter.MAX_PARTICLES = 25
        pf = ParticleFilter(self.map_, Logger())
        pf.pose_pub = Publisher()
        pf.particle_publisher = Publisher()

        pf.make_header = make_header
        pf.wait_sensor_msg = wait_sensor_msg

        # rospy.Subscriber("initialpose", PoseWithCovarianceStamped, pf.start_pose_callback, queue_size=1)
        # rospy.Subscriber("odom", Odometry, pf.odom_callback, queue_size=1)

        self.assertListEqual(pf.pose_pub.buffer, [])
        self.assertIsInstance(pf, ParticleFilter)

        self.pf = pf

    def test_parameters(self):
        self.assertIsInstance(self.pf.logger, Logger)

        self.assertIsNone(self.pf.sensor_data)

        # Don't need to check elements of cache (could be ones, zeros not matter)
        self.assertEqual(self.pf.odom_delta.size, 6)
        self.assertTupleEqual(self.pf.odom_cov.shape, (3, 3))
        self.pf.iters = 0

        self.assertIs(self.pf.map_handler, self.map_)
        self.assertEqual(self.pf.map_frame, "map")

        self.assertIsInstance(self.pf.inferred_pose, PoseWithCovarianceStamped)
        self.assertEqual(self.pf.particle_indices.size, ParticleFilter.MAX_PARTICLES)
        self.assertEqual(self.pf.particle_cloud.shape, (self.pf.MAX_PARTICLES, 3))
        self.assertEqual(self.pf.grid.size, self.pf.MAX_PARTICLES)

        # TODO: Find a way to test the Lock
        # self.assertIsInstance(self.pf.state_lock, Lock)

        self.assertIsNone(self.pf.last_time)
        self.assertIsNone(self.pf.last_stamp)
        self.assertIsNone(self.pf.last_pose)

        self.assertIsInstance(self.pf.pub_tf, TransformBroadcaster)

        self.assertEqual(self.pf.particles.shape, (ParticleFilter.MAX_PARTICLES, 3))
        # TODO: Test upper limit
        for pair in zip(self.pf.particles.max(axis=1), (self.map_.origin.x, self.map_.origin.y, 0)):
            self.assertGreaterEqual(pair[0], pair[1])

        self.assertAlmostEqual(self.pf.weights.sum(), 1.)
        self.assertEqual(self.pf.weights.size, ParticleFilter.MAX_PARTICLES)

    def test_sensor_model(self):
        self.pf.particles = self.particles_static.copy()
        previous = self.pf.weights.copy()
        # Test for sensor reading half intensity, should lead to uniform weights
        self.pf.sensor_data = 127.5
        self.pf.sensor_model()
        assert_array_equal(previous, self.pf.weights)
        # Test for sensor reading almost HIGH (3/4)
        self.pf.sensor_data = 3. * 255. / 4.
        self.pf.sensor_model()
        # Formula for the expectation is 0.75*15/(0.75*15 + 0.25*10)
        self.assertAlmostEqual((self.mask * self.pf.weights).sum(), 0.81818181)
        # Test same measurement again
        self.pf.sensor_data = 3. * 255. / 4.
        self.pf.sensor_model()
        # Formula for the expectation is 0.75*0.75*15/(0.75*0.75*15 + 0.25*0.25*10)
        self.assertAlmostEqual((self.mask * self.pf.weights).sum(), 0.93103448)
        previous = self.pf.weights.copy()
        # Do nothing if has no measurement available, also testing if it is clearing the measurements
        self.pf.sensor_model()
        assert_array_equal(previous, self.pf.weights)

    def test_normalize_weights(self):
        self.pf.weights = np.array([1., 1., 8.])
        self.pf.normalize_weights()
        assert_array_equal(self.pf.weights, (0.1, 0.1, 0.8))

    def test_update_sensor_data(self):
        self.pf.sensor_data = 99
        self.pf.update_sensor_data()
        self.assertEqual(123, self.pf.sensor_data)

    def test_mcl(self):
        # Without the motion and sensor updates the mcl method is just a resampling
        def empty():
            pass

        self.pf.motion_model = empty
        self.pf.sensor_model = empty
        self.pf.particles = self.particles_static.copy()
        last = self.pf.particles[-1].copy()
        initial_w = self.pf.weights.copy()
        self.pf.weights = np.ones(25, dtype=np.float)
        self.pf.weights[-1] = 100
        self.pf.normalize_weights()
        self.pf.mcl()
        # The last with more weights should appear more than 20 times
        self.assertGreaterEqual((self.pf.particles == last).sum(), 20)
        # The weights should be reset
        assert_array_equal(initial_w, self.pf.weights)

    def test_publish_particles(self):
        self.pf.particles = self.particles_static.copy()
        self.pf.publish_particles()
        self.assertEqual(len(self.pf.particle_publisher.buffer), 1)
        sample = self.pf.particle_publisher.buffer.pop()

        self.assertIsInstance(sample, PoseArray)
        self.assertEqual(len(sample.poses), ParticleFilter.MAX_PARTICLES)
        self.assertEqual(sample.header.frame_id, self.pf.map_frame)

        for particle, pose in zip(self.particles_static, sample.poses):
            self.assertEqual(particle[0], pose.position.x)
            self.assertEqual(particle[1], pose.position.y)
            self.assertEqual(particle[2], Utils.quaternion_to_angle(pose.orientation))

    def test_motion_model(self):
        # TODO: Fix motion model implementation
        # Do nothing when don't have a delta nor delta covariance
        self.pf.particles = self.particles_static.copy()
        self.pf.motion_model()
        assert_array_equal(self.pf.particles, self.particles_static)
        # TODO: Test for some fixed delta and see if it is working


if __name__ == '__main__':
    import rosunit

    rosunit.unitrun(PKG, NAME, TestParticleFilter)
