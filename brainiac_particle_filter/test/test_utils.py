# run test: nosetests
PKG = "brainiac_particle_filter"
NAME = "utils_test"

import roslib
import sys
import unittest
from brainiac_particle_filter.utils import *
from std_msgs.msg import Header
from geometry_msgs.msg import PoseWithCovarianceStamped, Pose, Quaternion, Point

# region variablesTest

ABSOLUTE_ERROR_TOL = 1e-02
RELATIVE_ERROR_TOL = 1e-02

# RPY: 60 30 30 (Degrees)
OBJECT_POSE_1 = PoseWithCovarianceStamped(header=Header(frame_id="OBJECT_POSE_1"),
                                          pose=Pose(position=Point(5, 5, 1),
                                                    orientation=Quaternion(0.5245191, 0.0915064, 0.3415064, 0.7745191)))

# RPY: 45 30 135 (Degrees)
OBJECT_POSE_2 = PoseWithCovarianceStamped(header=Header(frame_id="OBJECT_POSE_1"),
                                          pose=Pose(position=Point(3, 2, 1),
                                                    orientation=Quaternion(0.3623724, -0.25, 0.8623724, 0.25)))

ASSERT_MATRIX_POSE1 = [[0.7500000, -0.4330127, 0.5000000, 5],
                       [0.6250000, 0.2165063, -0.7500000, 5],
                       [0.2165063, 0.8750000, 0.4330127, 1],
                       [0.0000000, 0.0000000, 0.0000000, 1]]

ASSERT_MATRIX_POSE2 = [[-0.6123725, -0.6123725, 0.5000000, 3],
                       [0.2500000, -0.7500000, -0.6123725, 2],
                       [0.7500000, -0.2500000, 0.6123725, 1],
                       [0.0000000, 0.0000000, 0.0000000, 1]]

ASSERT_MATRIX_INV1 = np.linalg.inv(ASSERT_MATRIX_POSE1)
ASSERT_MATRIX_INV2 = np.linalg.inv(ASSERT_MATRIX_POSE2)
ASSERT_MATRIX_DIFF = np.matmul(ASSERT_MATRIX_INV2, ASSERT_MATRIX_POSE1)
ASSERT_POSE = np.array([5, 5, 1, 60, 30, 30])


# endregion

class TestUtils(unittest.TestCase):
    def test_angle_to_quaternion(self):
        angle = -1.98
        result = angle_to_quaternion(angle)
        self.assertIsInstance(result, Quaternion)
        self.assertAlmostEqual(0.0, result.x)
        self.assertAlmostEqual(0.0, result.y)
        self.assertAlmostEqual(-0.836026, result.z)
        self.assertAlmostEqual(0.5486899, result.w)

    def test_quaternion_to_angle(self):
        q = Quaternion(0, 0, -0.836026, 0.5486899)
        result = quaternion_to_angle(q)
        self.assertAlmostEqual(-1.98, result)

    def test_particle_to_pose(self):
        x = 1.2
        y = 3.4
        angle = -1.98
        result = particle_to_pose(x, y, angle)
        self.assertIsInstance(result, Pose)
        self.assertEqual(x, result.position.x)
        self.assertEqual(y, result.position.y)
        self.assertEqual(0., result.position.z)
        self.assertEqual(angle_to_quaternion(angle), result.orientation)

    def test_vector_particle_to_pose(self):
        list_particles = [[23.8, 213., -1.1], [41., -9., 1.4]]
        arr_particles = np.array(list_particles)
        result = vector_particle_to_pose(arr_particles[:, 0], arr_particles[:, 1], arr_particles[:, 2])
        self.assertEqual(particle_to_pose(*list_particles[0]), result[0])
        self.assertEqual(particle_to_pose(*list_particles[1]), result[1])

    def test_coord_to_indices(self):
        print("it converts a normal position")
        result = coord_to_indices(4.99, 1, 0.1, 100)
        self.assertEqual(39, result)
        print("it converts an extrapolated position")
        result = coord_to_indices(38.99, 1, 0.1, 100)
        self.assertEqual(99, result)
        result = coord_to_indices(0.99, 1, 0.1, 100)
        self.assertEqual(0, result)

    def test_vector_coord_to_indices(self):
        pos = np.array([4.99, 38.99, 0.99])
        result = vector_coord_to_indices(pos, 1, 0.1, 100)
        self.assertEqual(39, result[0])
        self.assertEqual(99, result[1])
        self.assertEqual(0, result[2])

    def test_odom_to_coordinates(self):
        odom = Odometry()
        odom.pose.pose.position = Point(51.6, 32.1, 14.7)
        odom.pose.pose.orientation = Quaternion(0., 0., 0.2425356, 0.9701425)
        result = odom_to_coordinates(odom)
        self.assertEqual(51.6, result[0])
        self.assertEqual(32.1, result[1])
        self.assertAlmostEqual(0.4899573, result[2])

    def test_mat_cov_from_plain(self):
        matrix = np.ones((3, 3))
        result = mat_cov_from_plain(list(range(36)), matrix)
        result2 = mat_cov_from_plain(list(range(36)))
        expected = [[0, 1, 5], [6, 7, 11], [30, 31, 35]]
        self.assertIs(matrix, result)
        self.assertIsNot(matrix, result2)
        for pair in zip(result.tolist(), result2.tolist(), expected):
            self.assertListEqual(pair[0], pair[-1])
            self.assertListEqual(pair[0], pair[1])

    def test_plain_cov_from_mat(self):
        matrix = np.array([[0, 1, 5], [6, 7, 11], [30, 31, 35]])
        out = np.ones(36)
        expected = [
            0, 1, 1, 1, 1, 5,
            6, 7, 1, 1, 1, 11,
            1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1,
            30, 31, 1, 1, 1, 35
        ]
        result = plain_cov_from_mat(matrix, out)
        result2 = plain_cov_from_mat(matrix)
        self.assertIs(out, result)
        self.assertIsNot(out, result2)
        self.assertListEqual(result.tolist(), result2.tolist())
        self.assertListEqual(result.tolist(), expected)

    def test_pose_to_transform_matrix_test(self):
        res_case_1 = pose_to_transform_matrix(OBJECT_POSE_1.pose.position, OBJECT_POSE_1.pose.orientation)
        res_case_2 = pose_to_transform_matrix(OBJECT_POSE_2.pose.position, OBJECT_POSE_2.pose.orientation)
        self.assertTrue(np.allclose(res_case_1, ASSERT_MATRIX_POSE1, rtol=ABSOLUTE_ERROR_TOL, atol=RELATIVE_ERROR_TOL))
        self.assertTrue(np.allclose(res_case_2, ASSERT_MATRIX_POSE2, rtol=ABSOLUTE_ERROR_TOL, atol=RELATIVE_ERROR_TOL))

    def test_h_transform_matrix_inv_test(self):
        res_case_1 = h_transform_matrix_inv(ASSERT_MATRIX_POSE1)
        res_case_2 = h_transform_matrix_inv(ASSERT_MATRIX_POSE2)
        self.assertTrue(np.allclose(res_case_1, ASSERT_MATRIX_INV1, rtol=ABSOLUTE_ERROR_TOL, atol=RELATIVE_ERROR_TOL))
        self.assertTrue(np.allclose(res_case_2, ASSERT_MATRIX_INV2, rtol=ABSOLUTE_ERROR_TOL, atol=RELATIVE_ERROR_TOL))

    def test_h_transform_diff_test(self):
        res_case_1 = h_transform_diff(OBJECT_POSE_2, OBJECT_POSE_1)
        self.assertTrue(np.allclose(res_case_1, ASSERT_MATRIX_DIFF, rtol=ABSOLUTE_ERROR_TOL, atol=RELATIVE_ERROR_TOL))

    def test_pose_from_h_transform(self):
        pose_container = np.zeros(6, np.float)
        pose_from_h_transform(np.array(ASSERT_MATRIX_POSE1), pose_container)
        self.assertTrue(np.allclose(pose_container, ASSERT_POSE, rtol=ABSOLUTE_ERROR_TOL, atol=RELATIVE_ERROR_TOL))


if __name__ == '__main__':
    import rosunit

    rosunit.unitrun(PKG, NAME, TestUtils)
